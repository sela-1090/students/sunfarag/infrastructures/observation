# Observation



## Prometheus and Grafana

create observation namespace if you dont have one already.
```
 kubectl create namespace observation
 kubectl get namespaces
```
add Prometheus and Grafana helm charts repositories
```
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add grafana https://grafana.github.io/helm-charts

```
update the local cache of Helm charts from the added repositories

```
 helm repo update
```
install Prometheus and Grafana using Helm in the "observation" namespace

```
 helm install prometheus prometheus-community/prometheus -n observation
 helm install grafana grafana/grafana -n observation

```

expose the Prometheus service using a NodePort service type

```
kubectl expose service prometheus-server --type=NodePort --target-port=9090 --name=prometheus-server-ext

```
retrieve information about the "grafana-ext" service
```
kubectl get svc grafana-ext -n observation

```
retrieve a secret named "grafana" in the "observation" namespace and display its details in YAML format

```
kubectl get secret -n observation grafana -o yaml

```
intended to decode and display the admin password stored in the "grafana" secret
```
kubectl get secret -n observation grafana -o jsonpath="{.data.admin-password}" | ForEach-Object { [System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String($_)) }

```
modify the port numbers of the services "grafana-ext" and "prometheus-server-ext"

```
kubectl patch service grafana-ext --type=json -p='[{"op": "replace", "path": "/spec/ports/0/port", "value": 3000}]' -n observation
kubectl patch service prometheus-server-ext --type=json -p='[{"op": "replace", "path": "/spec/ports/0/port", "value": 9090}]' -n observation

```
kubectl port-forward service/grafana-ext 3000:3000 -n observation
kubectl port-forward service/prometheus-server-ext 9090:9090 -n observation


open http://localhost:3000


Login into the Grafana dashboard with the user and paasword


run the commands

```
kubectl get pods -n observation
kubectl descirbe pod <pod name> -n observation

```

